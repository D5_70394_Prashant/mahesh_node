import { useEffect, useState } from "react";

function Book()
{
    var [bookname,setBookname]=useState("web programming");
    var [Auther,setAuther]=useState("mahesh");


useEffect(()=>
{
    console.log("in blank useeffect");
    },[])


useEffect(()=>
{
    console.log("in bookname update useeffect");
    },[bookname])


useEffect(()=>
{
    console.log("in Auther update useeffect");
    },[Auther])


useEffect(()=>
{
    console.log("in bookname or Auther update useeffect");
    },[bookname,Auther])




var Change=()=>
{
    setBookname("Core java");
    setAuther("nilesh");
}


        return <div>
                    <h4>
                        Bookname={bookname}
                    </h4>
                    <h4>
                        Auther={Auther}
                    </h4>
                    <button onClick={Change}>
                        Check Lifecycle
                    </button>
                </div>
}
export default Book;


//version3
// import { useState } from "react";
// function Book()
// {
//     var [books,setBooks]=
//         useState(
//             [
//                 {id: 11, bookName: "Let US C", bookAuthor: "Kanitkar"},
//                 {id: 12, bookName: "Let US CPP", bookAuthor: "Kanitkar"},
//                 {id: 13, bookName: "Let US OS", bookAuthor: "ABC"},
//                 {id: 14, bookName: "Let US Java", bookAuthor: "PQR"},
//                 {id: 15, bookName: "Let US MERN", bookAuthor: "XYZ"},
//                 {id: 16, bookName: "Let US DB", bookAuthor: "UVW"}
//             ]
//         );

// var Add=()=>
// {
//     var copyofBooks=[...books]
//     copyofBooks.push({id: 17, bookName: "ABCD", bookAuthor: "XYZ"});
//     setBooks(copyofBooks);
// }

// return  <div>
//             {
//                 books.map(book=>
//                     {
//                         return  <h3 key={book.id}>
//                                     Bookname={book.bookName}   
//                                     Auther ={book.bookAuthor}
//                                 </h3>
                               
//                     })
               

//             }
//              <br/>
//              <button onClick={Add}>Add Record</button>
//         </div>
        
// }
// export default Book;






//=============================================================
//import { useState } from "react";
// function Book()
// {
//     var [bookdetails,setBookdetails]= useState({bookName:"let us c",
//                                                 bookAuther:"kanetkar"})
            
//     var Change=()=>
//     {   
//         var copyofBookDetails= {...bookdetails,bookName:"CPP"}
//         setBookdetails(copyofBookDetails);
//     }


//     return  <div>
//                 <h1> bookname= {bookdetails.bookName}</h1>
//                 <br/>
//                 <h1> auther= {bookdetails.bookAuther}</h1>
//                 <br/>
//                 <button onClick={Change}>Change</button>
//             </div>
// }
// export default Book;




//version1
// import { useState } from "react";

// function Book ()
// {
//    var [myname,setMyname]= useState("prash");

// var change=()=>
// {
//     setMyname("pratik")
// }
//    return   <div>
//                 Name:{myname}
//                 <br/>
//                 <button onClick={change} >Change</button>
//             </div>
// }

// export default Book;