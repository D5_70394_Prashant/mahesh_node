import Dashboard from "./404Dashboard";
import Header from "./303Header";
import Footer from "./303Footer";
import { Link, Route, Switch } from "react-router-dom";
import Contact from "./402Contact";
import About from "./402About";
import Home from "./402Home";
import Login from "./403login";
import ProtectedRout from "./401ProtectedRoute";
import ProtectedRoute from "./401ProtectedRoute";


function MainUI()
{//debugger;
   return <div>
           <Header></Header>
            <br/>
            <hr/>
            <Link to={'/home'}>Home</Link>{"   "}
            <Link to={'/about'}>About</Link>{"   "}
            <Link to={'/dashboard'}>Dashboard</Link>{"   "}
            <Link to={'/contact'}>Contact</Link>{"   "}
            <Link to={'/login'}>Login</Link>{"   "}
            
            <Switch>
                <Route path='/' exact component={Home} ></Route>
                <Route path='/home' exact component={Home} ></Route>
                <Route path='/about' exact component={About} ></Route>
                <ProtectedRoute path='/dashboard' exact component={Dashboard} ></ProtectedRoute>
                <Route path='/contact' exact component={Contact} ></Route>
                <Route path='/login' exact component={Login} ></Route>
                <Route path='/*' exact component={Home} ></Route>
            </Switch>
            <br/>
            <Footer></Footer>
   </div> 
}

export default MainUI;