 import React from 'react';
 import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

// //import Company from './04company';
// import Sample from './05sample';
// import Lifecycle from './05show';
// import Users from './06Users';
// import Sunbeam from './07Users_bootstrap';
//import Register from './301Register';
//import Dashboard from './404Dashboard';
import Display from './303Display';
import Book from './304Book';
import MainUI from './401mainUI';
import Dashboard from './302Dashboard';
import Testaxios from './501TestAxios';
//import Parent from './parent';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
 <BrowserRouter>
      <MainUI></MainUI>
 </BrowserRouter>

//<Testaxios></Testaxios>

//<Dashboard></Dashboard>
  // <Book></Book>
//<Display></Display>
// <Dashboard></Dashboard>
  //<Register></Register>
  //<Parent></Parent>   //give call to class componant to be rendered
 //<Company></Company>
 //<Lifecycle></Lifecycle>  
 //<Sample></Sample>
 //<Users></Users>
// <Sunbeam></Sunbeam>
);


