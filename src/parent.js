import React, { Component } from 'react';
import Child from './child';

class Parent extends Component {
    state = { cname:"prash" } 
    change=()=>{
        this.setState({cname:"pratik"})
    }

    render() { 

        return (
            <div>
                <Child mydata={this.state.cname}  f={this.change}/>
            {/* <h1>Name= {this.state.cname}</h1> */}

            </div>
        );
    }
}
 
export default Parent;