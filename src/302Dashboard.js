import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import './06common.css'

class Dashboard extends Component {
    state = { employees: 
        [
                    {No: 11, Name: "Sachin", City:"Pune"},
                    {No: 12, Name: "Amit", City:"Mumbai"},
                    {No: 13, Name: "Nilesh", City:"Chennai"},
                    {No: 14, Name: "Rohan", City:"Panji"},
                    {No: 15, Name: "Madhura", City:"Mumbai"},
                    {No: 16, Name: "Mahesh", City:"Pune"},
                    {No: 17, Name: "Rahul", City:"Pune"}
         ]   ,
         employee:{No:"", Name: "", City:""}
        }; 

HandleChange=(args)=>{
    var copyofEmployee={...this.state.employee}
    copyofEmployee[args.target.name]=args.target.value
    this.setState({employee:copyofEmployee})
}        
Add=()=>
{  // debugger;

    var copyofEmployees=[...this.state.employees]
    copyofEmployees.push(this.state.employee)
    this.setState({employees:copyofEmployees})
    this.setState({employee:{No:"", Name: "", City:""}})
}

Clean=()=>
{//debugger;
    this.setState({employee: {No: "", Name: "", City:""}});
}
Remove=(no)=>{
    var filteredEmployees=this.state.employees.filter((emp)=>
    {
        return (emp.No !=no)
    })

    this.setState({employees:filteredEmployees})
}


render() { 
    return (
        <div>
            <table className='table table-responsive registerTable' >
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>
                            <input type={"text"}
                                    name="No"
                                    value={this.state.employee.No}
                                    onChange={this.HandleChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>
                            <input type={"text"}
                                    name="Name"
                                    value={this.state.employee.Name}
                                    onChange={this.HandleChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>
                            <input type={"text"}
                                    name="City"
                                    value={this.state.employee.City}
                                    onChange={this.HandleChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={"2"}>
                            <button className='btn btn-primary' 
                                    onClick={this.Add}>
                                Add Record
                            </button>
                            {"                     "}
                            <button className='btn btn-info' 
                                    onClick={this.Clean}>
                                Clear
                            </button>
                        </td>
                    </tr>
                </tbody>

            </table>
            
            <hr></hr>
            <table className='table table-responsive table-dark
                            table-stripped table-hover'>
             <tbody>
                        {
                        this.state.employees.map((emp)=>
                            {
                                // debugger;
                                return (<tr key={emp.id}>
                                    <td>{emp.No}</td>
                                    <td>{emp.Name}</td>
                                    <td>{emp.City}</td>
                                    <td>

                                        <button className='btn btn-outline-danger'
                                        onClick={
                                            ()=>{this.Remove(emp.No)}
                                        }
                                        >Delete</button>
                                    </td>
                                </tr>)

                            })
                             
                        }
                    </tbody>

        </table>
        </div>
    );
}
}
 
export default Dashboard;