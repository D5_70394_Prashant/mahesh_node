import React, { Component } from 'react';
import './06common.css'

class Users extends Component {
    state = { allusers: [] } 

    componentDidMount()
    {
        var helper=new XMLHttpRequest();
        helper.onreadystatechange=()=>{
            if(helper.readyState==4 && helper.status==200)
            {
              var result= JSON.parse( helper.responseText);
              this.setState({allusers:result.data});
            }
        }
        helper.open("GET","https://reqres.in/api/users");
        helper.send();
    }



    render() { 
        return (
            <div>
                <center><h1>Welcome to user page</h1></center>
            
            <hr/>
            <div className='container' >
                {this.state.allusers.map(user=>
                {
                    return(
                        <div key={user.id} className='content'>
                            <img src={user.avatar} className='photo' />
                            <br/>
                            <h2>{user.first_name}.{user.last_name}</h2>
                        </div>
                    )
                })
                }



            </div>
            </div>

        );
    }
}
 
export default Users;