import React, { Component } from 'react';
import Parent from './parent';
class Child extends Component {
    
    constructor(props)
    {
        super(props)
    }

    render() { 
        return (
            <>
            <h1>{this.props.mydata}</h1>
            <button onClick={this.props.f}>click me</button>
            </>
        );
    }
}
 
export default Child;