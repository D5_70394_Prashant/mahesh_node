
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import './06common.css'

function Dashboard () {
     

var [employees,setEmployees]=useState([
                {No: 11, Name: "Sachin", City:"Pune"},
                {No: 12, Name: "Amit", City:"Mumbai"},
                {No: 13, Name: "Nilesh", City:"Chennai"},
                {No: 14, Name: "Rohan", City:"Panji"},
                {No: 15, Name: "Madhura", City:"Mumbai"},
                {No: 16, Name: "Mahesh", City:"Pune"},
                {No: 17, Name: "Rahul", City:"Pune"}
                ]);
var [employee,setEmployee] = useState({No:"", Name: "", City:""});
var [searchText,setsearchText]=useState("");

var history=useHistory();
var [message,setMessage]=useState("Collection Changed")

var Search=(args)=>
{
    setsearchText(args.target.value);
}

var ClearSearch=()=>
{
    setsearchText("");
}

useEffect(()=>{
    setMessage("collection change!")
    setTimeout(() => { 
        setMessage("")
    },1000)
},[employees])

var HandleChange=(args)=>{
    var copyofEmployee={...employee};
    copyofEmployee[args.target.name]=args.target.value;
    setEmployee(copyofEmployee);
}        
var Add=()=>
{  // debugger;

    var copyofEmployees=[...employees]
    copyofEmployees.push(employee)
   setEmployees(copyofEmployees)
    setEmployee({No:"", Name: "", City:""})
}

var Clean=()=>
{//debugger;
    setEmployee({No: "", Name: "", City:""});
}
var Remove=(no)=>{
    var filteredEmployees=employees.filter((emp)=>
    {
        return (emp.No !=no)
    })

    setEmployees(filteredEmployees)
}

var Logout = ()=>{
    sessionStorage.removeItem("isLoggedin");
    sessionStorage.removeItem("userName");
    history.push("/dashboard");
}
   

    return (
        <div className='table-responsive'>
            <div style={{float: "right"}}>
                        <button className='btn btn-primary' 
                                onClick={Logout}>
                            Log out
                        </button>
                    </div>
           <center>
           <table className='table table-responsive registerTable' >
                <tbody>
                    <tr>
                        <td>No</td>
                        <td>
                            <input type={"text"}
                                    name="No"
                                    value={employee.No}
                                    onChange={HandleChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>
                            <input type={"text"}
                                    name="Name"
                                    value={employee.Name}
                                    onChange={HandleChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>
                            <input type={"text"}
                                    name="City"
                                    value={employee.City}
                                    onChange={HandleChange}/>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={"2"}>
                            <button className='btn btn-primary' 
                                    onClick={Add}>
                                Add Record
                            </button>
                            {"                     "}
                            <button className='btn btn-info' 
                                    onClick={Clean}>
                                Clear
                            </button>
                        </td>
                    </tr>
                </tbody>

            </table>

            
           </center>
            
            
            <hr></hr>
            <div className='content'>
            <hr/>
                Search By City <input type={"text"}
                                name="searchText"
                                value={searchText}
                                onChange={Search} />
                    {" "}
                    <button className='btn btn-info' 
                                    onClick={ClearSearch}>
                                Clear
                            </button>
                    
                    <hr></hr>
                        <div className='alert alert-info' ><b>{message}</b></div>
                     <hr></hr>
            </div>
            <table className='table table-responsive table-dark
                            table-stripped table-hover'>
               

             <tbody>
                        {
                        employees.map((emp)=>
                            {
                                // debugger;
                                if (emp.City.toLowerCase().includes(searchText.toLowerCase()))
                                {
                                    return (<tr key={emp.id}>
                                                <td>
                                                    {emp.No}
                                                </td>
                                                <td>
                                                    {emp.Name}
                                                    </td>
                                                <td>
                                                    {emp.City}
                                                </td>
                                                <td>
                                                    <button className='btn btn-outline-danger'
                                                    onClick={
                                                        ()=>{Remove(emp.No)}
                                                    }
                                                    >Delete</button>
                                                </td>
                                            </tr>)

                                }
                                

                            })
                             
                        }
                    </tbody>

        </table>
        </div>
    );
}

 
export default Dashboard;

