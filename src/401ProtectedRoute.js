import { Route, useHistory } from "react-router-dom";
import Login from "./403login";


function ProtectedRoute(props)
{
    var isLoggedin="false";
   // debugger;

    var loginStatusFromSessionStorage=sessionStorage.getItem("isLoggedin");
    if (loginStatusFromSessionStorage!=null)
    {
        if (loginStatusFromSessionStorage=="true")
        {
            isLoggedin="true";
        }
    
    }


    if (isLoggedin == "true")
    {
        return <Route path={props.path} exact component={props.component}/>
    }
    else
    {
        return <Login></Login>
    }
}

export default ProtectedRoute;