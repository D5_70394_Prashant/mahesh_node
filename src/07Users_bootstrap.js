import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import './06common.css'
class Sunbeam extends Component {
    state = { allusers: [] } 

    componentDidMount()
    {
        var helper=new XMLHttpRequest();
        helper.onreadystatechange=()=>{
            if(helper.readyState==4 && helper.status==200)
            {
              var result= JSON.parse( helper.responseText);
              this.setState({allusers:result.data});
            }
        }
        helper.open("GET","https://reqres.in/api/users");
        helper.send();
    }



    render() { 
        return (
        <div >
                <center><h1>Welcome to user page</h1></center>
            <hr/>    
            <button className='btn btn-primary' >Create New Record</button>
            <hr/>
                <div className='table-responsive content'>
                    <table className='table table-bordered  table-dark
                                        table-stripped table-hover'>
                        <tbody>
                        {
                        this.state.allusers.map((user)=>
                            {
                                // debugger;
                                return (<tr key={user.id}>
                                    <td>{user.id}</td>
                                    <td>{user.first_name}</td>
                                    <td>{user.last_name}</td>
                                    <td>{user.email}</td>
                                    <td>
                                    <button className='btn btn-outline-warning' >
                                        Edit
                                        </button>  
                                    </td>
                                    <td>
                                    <button className='btn btn-danger btn-sm' >
                                        Delete
                                        </button>  
                                    </td>
                                </tr>)

                            })
                             
                        }
                    </tbody>
                </table>

                </div>
        </div>

        );
    }
}
 
export default Sunbeam;

{/* <div className='table-responsive'>
                    <table className='table table-bordered'></table> */}
 //div which conatain table use table-responsive to get                   
