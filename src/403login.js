import { useState } from "react"
import { useHistory } from "react-router-dom";
import './06common.css';


function Login()
{  // debugger;
    var [user,setUser]=useState({userName:"",password:""})
    var [message,setMessage]=useState("");
    var history = useHistory();



var HandleChange=(args)=>
{
    var copyofUser={...user};
    copyofUser[args.target.name]=args.target.value;
    setUser(copyofUser);
}


var Signin=()=>
{//debugger;
    if (user.userName=="prash" && user.password =="007")
    {   setMessage("")
        sessionStorage.setItem("isLoggedin","true");
        sessionStorage.setItem("userName","prash");
        history.push('/dashboard');

    }
    else
    {
       // console.log("wrong credentials")
        setMessage("wrong credentials")
        setUser({userName:"",password:""})
    }
}
    
    return  <div>
                <table className="table table-responsive registerTable">
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>
                                <input type={"text"} 
                                        name="userName"
                                        onChange={HandleChange}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>
                                <input type={"text"} 
                                        name="password"
                                        onChange={HandleChange}/>
                            </td>
                        </tr>
                        
                        <tr className="content">
                            <td colSpan={2}>
                                <button className="btn btn-primary" onClick={Signin}>LOGIN</button>
                            </td>
                        </tr>
                    </tbody> 
                </table>
                <div className="content registerTable">
                    <h5>{message}</h5>
                   
                </div>

            </div>
}
export default Login;